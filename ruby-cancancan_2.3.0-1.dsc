Format: 3.0 (quilt)
Source: ruby-cancancan
Binary: ruby-cancancan
Architecture: all
Version: 2.3.0-1
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Sriram and Priyadharshini <sriram99.rhss@gmail.com>
Homepage: https://github.com/CanCanCommunity/cancancan
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-cancancan
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-cancancan.git
Testsuite: autopkgtest-pkg-ruby
Build-Depends: debhelper (>= 11~), gem2deb
Package-List:
 ruby-cancancan deb ruby optional arch=all
Checksums-Sha1:
 d3e839e5399cc3d33d6a9963ec9ac6642434afa1 19404 ruby-cancancan_2.3.0.orig.tar.gz
 bda5f8532c75f0f6b170d554ce034f0c7240f936 1812 ruby-cancancan_2.3.0-1.debian.tar.xz
Checksums-Sha256:
 ab17e7670784096d5714713fae55ee4fc10a1ef22fc317480bc40a4d188bdf47 19404 ruby-cancancan_2.3.0.orig.tar.gz
 46d14a07ecf4817f831f6d7e0421efd21638dd74082c56cae2f9039fd5446dd2 1812 ruby-cancancan_2.3.0-1.debian.tar.xz
Files:
 6084ea9dd31bc014cdec3ab9f6891659 19404 ruby-cancancan_2.3.0.orig.tar.gz
 678249d1f33b43082fab57d98fc5fa4d 1812 ruby-cancancan_2.3.0-1.debian.tar.xz
Ruby-Versions: all
